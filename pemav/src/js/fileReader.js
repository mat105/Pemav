const fs = require('fs');
const glob = require('glob');
const pathModule = require('path');

function findStoriesOnDisk() {
    const stories = glob.sync(pathModule.join('stories', '**', '*.json'));

    return stories.map((st) => { return {dir: pathModule.dirname(st), story: readFile(st)} });
}

function readFile(path) {
    return JSON.parse(fs.readFileSync(path, 'utf8'));
}

export {
    findStoriesOnDisk
};
