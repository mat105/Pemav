import Story from './Story.js';
import { mkel } from './utils.js';
import { findStoriesOnDisk } from './fileReader.js';

const LIBRARY_ID = 'story-library';
const LIBRARY_STACK = 'story-stack';

export default class Library {
    constructor(player) {
        this._player = player;
        this._stories = findStoriesOnDisk().map((st) => {return new Story(player, st.dir, st.story)});
    }

    clear() {
        document.getElementById(LIBRARY_ID).innerHTML = "";
    }

    drawCard(story) {
        window._sstory = story;
        let img = mkel('img', {attributes: {'src': story.cover}}),
        aimg = mkel('a', {attributes: {href: '#'}, children: [img]}),
        cover = mkel('div', {classes: ['card-cover'], children: [aimg]}),
        content = mkel('div', {content: story.title, classes: ['card-content']}),
        card = mkel('div', {classes: ['story-card'], children: [cover, content]});

        aimg.addEventListener('click', (ev) => (this._player.load(story)));

        document.getElementById(LIBRARY_STACK).appendChild(card);
    }

    draw() {
        this._stories.forEach(st => {
            this.drawCard(st);
        });
    }
}
