import Element from './Element.js';

export default class SoundElement extends Element {
    constructor(story, data) {
        super(story, data);

        this.sound = new Howl({
            src: [this.__source],
            volume: this.__volume || 1,
            loop: this.__loop || false
        });

        this.sound.on('end', () => { this._finished = true;});
    }

    play() {
        this._finished = false;
        this.sound.play();
        if (this.__startPosition) {
            this.sound.seek(this.__startPosition);
        }
    }

    onEnter() {
        this.play();
    }

    stop() {
        this.sound.stop();
    }

    onExit() {
        this.stop();
    }

    onPause(state) {
        if (state) {
            this.sound.pause();
        } else if (!this._finished && !this.sound.playing()) {
            console.log("played");
            this.sound.play();
        }
    }
};
