import VisualElement from './VisualElement.js';

const DEFAULT_COLOR = 'antique-white';
const DEFAULT_FONT_SIZE = '16pt';

const COMMA_TIME_MULT = 6;
const NEWLINE_TIME_MULT = 6;
const DOT_TIME_MULT = 10;

export default class TextElement extends VisualElement {

    constructor(story, data) {
        super(story, data);

        this.addStyle('color', this.__color || DEFAULT_COLOR);
        this.addStyle('font-size', this.__fontSize || DEFAULT_FONT_SIZE);

        this.__content = this.__content || '';

        if (this.__drawTime) {
            this.calcDrawVars();
        }
    }

    calcDrawVars() {
        this._drawVars = {
            commas: this.__content.match(/,/g || []).length,
            dots: this.__content.match(/\./g || []).length,
            newlines: this.__content.split('\n').length
        };

        this._drawVars.chars = this.__content.length - (this._drawVars.commas + this._drawVars.dots + this._drawVars.newlines);

        this._drawVars.charTime = 1000*(this.__drawTime / (this._drawVars.chars + COMMA_TIME_MULT * this._drawVars.commas + DOT_TIME_MULT * this._drawVars.dots + NEWLINE_TIME_MULT * this._drawVars.newlines));
    }

    addChar() {
        let newChar, waitTime, commas, newlines, dots;
        if (this._drawVars.currentPos < this.__content.length) {
            waitTime = this._drawVars.charTime;
            newChar = this.__content[this._drawVars.currentPos];
            this._drawVars.realString += newChar;
            this._drawVars.currentPos += 1;

            this.HTMLElement.innerText = this._drawVars.realString;

            if (newChar === ',') {
                waitTime *= COMMA_TIME_MULT;
            } else if (newChar === '\n') {
                waitTime = NEWLINE_TIME_MULT;
            } else if (newChar === '.') {
                waitTime *= DOT_TIME_MULT;
            }

            this._drawVars.drawTimeout = setTimeout(() => this.addChar(), waitTime);
        }
    }

    onExit() {
        if (this.__drawTime && this._drawVars.drawTimeout) {
            clearTimeout(this._drawVars.drawTimeout);
            this._drawVars.drawTimeout = null;
        }
    }

    onPause(state) {
        if (state && this.__drawTime && this._drawVars.drawTimeout) {
            clearTimeout(this._drawVars.drawTimeout);
            this._drawVars.drawTimeout = null;
        } else if (!state && this.__drawTime) {
            this.addChar();
        }
    }

    play() {
        if (this.__drawTime) {
            this.HTMLElement.innerText = "";
            this._drawVars.currentPos = 0;
            this._drawVars.realString = "";
            this._drawVars.drawTimeout = null;
            this.addChar();
        }
    }

    get ID() {
        return 'story-text';
    }

    get containerID() {
        return 'text-container';
    }

}
