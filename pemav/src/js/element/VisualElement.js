import Element from './Element.js';

/* Elemento que tiene un objeto HTML como objetivo */
export default class VisualElement extends Element {
    constructor(story, data) {
        super(story, data);
        this._drawingAttrs = {};
    }

    addStyle(name, value) {
        this._drawingAttrs[name] = value;
    }

    get ID() {}
    get HTMLElement() {
        return document.getElementById(this.ID);
    }
    get containerID() {
        return 'visual-container';
    }
    get container() {
        return document.getElementById(this.containerID);
    }
    onDoubleClick() {
        this._story.player.toggleElementsHideExcept(this);
    }
    get HTMLType() {
        return 'div';
    }
    createHTMLElement(elemType, attrs) {
        let element = this.HTMLElement;

        if (element) {
            element.parentNode.removeChild(element);
        }

        if (!elemType) {
            return null;
        }

        element = document.createElement(elemType || 'div');
        element.id = this.ID;

        if (this.__content) {
            element.innerText = this.__content;
        }

        if (this.__source) {
            element.src = this.__source;
        }

        if (attrs) {
            for (let key of Object.keys(attrs)) {
                element.style[key] = attrs[key];
            }
        }

        element.draggable = false;

        this.container.appendChild(element);
        this.container.style.display = '';

        return element;
    }

    draw() {
        this.createHTMLElement(this.HTMLType, this._drawingAttrs);
    }

    onEnter() {
        this.draw();
        this.play();
    }

    onExit() {
        this.clear();
    }

    onPause(state) {
        if (state === true) {
            this.pause();
        } else {
            this.play();
        }
    }

    play() {
        const elem = this.HTMLElement;
        if (elem && typeof elem.play === "function") {

            if (typeof elem.paused !== 'undefined' && elem.paused === false) {
                if (typeof elem.currentTime !== 'undefined' && this.__startPosition) {
                    elem.currentTime = this.__startPosition;
                }

                if (typeof elem.volume !== 'undefined' && this.__volume != null) {
                    elem.volume = this.__volume;
                }
            }

            elem.play();
        }
    }

    pause() {
        const elem = this.HTMLElement;
        if (elem && typeof elem.pause === "function") {
            elem.pause();
        }
    }

    clear() {
        const elem = this.HTMLElement;
        if (elem && typeof elem.pause === "function") {
            elem.pause();
            if (typeof elem.currentTime !== 'undefined' && this.__startPosition != null) {
                elem.currentTime = this.__startPosition;
            }
        }
    }
}
