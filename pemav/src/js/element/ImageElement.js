import VisualElement from './VisualElement.js';

export default class ImageElement extends VisualElement {

    constructor(story, data) {
        super(story, data);

        if (this.__blackAndWhite) {
            this.addStyle('filter', 'grayscale(1)');
        }
    }

    get ID() {
        return 'story-image';
    }

    get HTMLType() {
        return 'img';
    }
}
