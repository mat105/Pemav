import VisualElement from './VisualElement.js';

export default class VideoElement extends VisualElement {
    get ID() {
        return 'story-video';
    }

    get HTMLType() {
        return 'video';
    }

}
