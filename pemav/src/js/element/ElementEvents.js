export default {
    EVENT_EXPAND: 1,
    EVENT_DISABLE_MOUSEDOWN: 2,
    ALL_EVENTS: [1, 2]
};