import Element from './Element.js';
import TextElement from './TextElement.js';
import ImageElement from './ImageElement.js';
import SoundElement from './SoundElement.js';
import VideoElement from './VideoElement.js';
import MusicElement from './MusicElement.js';

const ELEMENT_MAPPER = {
    'text': TextElement,
    'image': ImageElement,
    'sound': SoundElement,
    'video': VideoElement,
    'music': MusicElement
};

const DEFAULT_CLASS = Element;

export default class ElementFactory {
    static getNew(elementClass, story, data) {
        const targetClass = ELEMENT_MAPPER[elementClass] || DEFAULT_CLASS;

        return new targetClass(story, data);
    }
}
