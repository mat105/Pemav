export default class Element {
    constructor(story, data) {
        this._story = story;
        this._data = data;
        this.parseArgs(data);
    }

    parseArgs(data) {
        const attrs = Object.keys(data);

        attrs.forEach(attr => {
            this[`__${attr}`] = data[attr];
        });

        if (data.source) {
            this.__source = this._story.resourceDir + '/' + this.__source;
        }
    }

    onEnter() {
    }

    onExit() {
    }

    onPause(state) {
    }

    hasAttribute(attr) {
        return typeof this[`__${attr}`] !== 'undefined' && this[`__${attr}`] !== null;
    }
}
