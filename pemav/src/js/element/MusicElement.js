import SoundElement from './SoundElement.js';

/* Music lives through succesive pages.
It is played onEnter by the page, but controlled by the player afterwards */
export default class MusicElement extends SoundElement {
    onExit() {}
}
