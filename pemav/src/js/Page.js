import ElementFactory from './element/ElementFactory.js'

export default class Page {
    constructor(story, pageData) {
        this._elements = [];

        for (const key of Object.keys(pageData)) {
            const elem = ElementFactory.getNew(key, story, pageData[key]);
            this._elements.push(elem);
            this[key] = elem;
        }
    }

    get elements() {
        return this._elements;
    }

    getElementByStringType(type) {
        return this[type];
    }
}
