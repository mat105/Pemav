import Page from './Page.js';

export default class Story {

    constructor(player, filePath, pemData) {
        this.pemData = pemData;
        this.player = player;
        this.resourceDir = filePath;
    }

    get storyData() {
        return this.pemData.story;
    }

    load() {
        this._currentPage = -1;
        this._pages = this.storyData.content.pages.map((page) => new Page(this, page));
    }

    unload() {
        this._pages = [];
    }

    get cover() {
        return this.resourceDir + '/' + this.storyData.metadata.cover;
    }

    get title() {
        return this.storyData.metadata.title;
    }

    getOption(name) {
        return this.storyData.options[name];
    }

    get language() {
        return this.storyData.metadata.language || 'es-ES';
    }

    get currentPage() {
        return this._pages[this._currentPage] || null;
    }

    get currentPageNumber() {
        return this._currentPage;
    }

    setPage(i) {
        if (this.isValidPage(i)) {
            this._currentPage = i;
            return true;
        }

        return false;
    }

    isValidPage(i) {
        return (i >= 0 && i < this._pages.length);
    }

    get elements() {
        return this.currentPage ? this.currentPage.elements : [];
    }

}
