import Story from './Story.js';
import { byID, mkel } from './utils.js';

export default class WebPlayer {
    constructor(targetDIV) {
        this._browserSpeech = 'speechSynthesis' in window;

        this._divID = targetDIV;
        this._volumeOff = false;

        this._preload();
    }

    generateDIVStructure() {
        let vc, tc, pc, imgs, targetDiv, sd;
        if (this.alreadyGenerated) {
            return;
        }

        imgs = [
            mkel('img', {id: 'goto-library', attributes:{src: "icons/library.png"}}),
            mkel('img', {id: 'prev-page', attributes:{src: "icons/prev-page.png"}}),
            mkel('img', {id: 'play-pause', attributes:{src: "icons/pause.png"}}),
            mkel('img', {id: 'stop', attributes:{src: "icons/stop.png"}}),
            mkel('img', {id: 'next-page', attributes:{src: "icons/next-page.png"}}),
            mkel('img', {id: 'volume', attributes:{src: "icons/volume-on.png"}})
        ];

        tc = mkel('div', {id: "text-container", classes: ['pem-container']});
        vc = mkel('div', {id: "visual-container", classes: ['pem-container'], children: [tc]});

        pc = mkel('div', {id: "player-controls", children: imgs});

        targetDiv = byID(this._divID);
        targetDiv.innerHTML = "";

        sd = mkel('div', {id: "story-div", children: [vc]})

        targetDiv.appendChild(sd);
        targetDiv.appendChild(pc);

        this._initEvents();
        this.alreadyGenerated = true;
    }

    _preload() {
        /* Corrige bug con speech synthesis en su primer pedido */
        const msg = new SpeechSynthesisUtterance("$");
        window.speechSynthesis.speak(msg);
        window.speechSynthesis.cancel();
    }

    get textContainer() {
        return byID('text-container');
    }

    get visualContainer() {
        return byID('visual-container');
    }

    _initEvents() {
        const controls = byID('player-controls');
        byID('visual-container').addEventListener('dblclick', () => {
            let el = this.textContainer;
            el.style.display = el.style.display === 'none' ? '' : 'none';
        });

        byID('prev-page').addEventListener('click', () => {
            this.prevPage();
        });

        byID('next-page').addEventListener('click', () => {
            this.nextPage();
        });

        byID('play-pause').addEventListener('click', () => {
            this.togglePause();
        });

        byID('volume').addEventListener('click', () => {
            this.toggleVolume();

            byID('volume').setAttribute('src', this._volumeOff ? 'icons/volume-off.png' : 'icons/volume-on.png');
        });

        byID('stop').addEventListener('click', () => {
            this.stop();
        });
    }

    loadStory(pem, resourceDir) {
        let st;
        if (typeof pem === "string") {
            pem = JSON.parse(pem);
        }
        st = new Story(this, resourceDir || '', pem);

        this.load(st);
    }

    load(story) {
        this.generateDIVStructure();
        //this.pemData = story.pemData;

        // Revisar el true
        this.clear(true);

        if (this._story) {
            /*if (this._currentMusic) {
                this._currentMusic.stop();
                this._currentMusic = null;
            }*/
            this._story.unload();
        }

        this._story = story;
        this._story.load();
        this.setPage(0);
    }

    onlyText() {
        return 0 === this._story.elements.filter(elem => {return elem.containerID !== 'text-container'}).length;
    }

    play() {
        let music = this._story.currentPage.getElementByStringType('music');

        if (music) {
            if (this._currentMusic) {
                this._currentMusic.stop();
            }

            this._currentMusic = music;
        }

        this._story.elements.forEach(element => {
            element.onEnter();
        });

        if (this.onlyText()) {
            this.textContainer.classList.toggle('expand');
        } else {
            this.textContainer.classList.remove('expand');
        }

    }

    pause(paused) {
        this._paused = paused;

        byID('play-pause').setAttribute('src', this._paused ? 'icons/play.png' : 'icons/pause.png');

        if (this._paused && window.speechSynthesis.speaking === true) {
            window.speechSynthesis.pause();
        }

        if (this._currentMusic) {
            this._currentMusic.onPause( this._paused );
        }

        if (this._story) {
            this._story.elements.forEach(element => {
                element.onPause(this._paused);
            });
        }

    }

    togglePause() {
        if (this._story) {
            this.pause(!this._paused);
        }
    }

    toggleVolume() {
        this._volumeOff = !this._volumeOff;
        if (this._volumeOff) {
            Howler.volume(0);
        } else {
            Howler.volume(1);
        }
    }

    clear(pauseMusic) {
        if (this._story && this._story.elements) {
            this._story.elements.forEach(e => e.onExit());
        }
        
        let txem = byID('text-container');

        document.querySelectorAll('.pem-container').forEach(e => {
            e.innerHTML = '';
            e.classList.remove('expand');
        });

        byID('visual-container').appendChild(txem);

        if (pauseMusic === true) {
            if (this._currentMusic) {
                this._currentMusic.stop();
                this._currentMusic = null;
            }
        }

        this._paused = false;
    }

    stop() {
        this.setPage(this._story.currentPageNumber);
        this.pause(true);
    }

    speechEnabled() {
        if (this._story) {
            return this._browserSpeech && this._story.getOption('allowBrowserSpeech') === true;
        }

        return this._browserSpeech;
    }

    speakText() {
        if (this._story && !this._paused) {
            const textElem = this._story.currentPage.getElementByStringType('text');

            if (textElem && textElem.__content) {
                const msg = new SpeechSynthesisUtterance(textElem.__content);
                msg.lang = this._story.language;
                if (window.speechSynthesis.paused) {
                    window.speechSynthesis.resume();
                } else if (window.speechSynthesis.speaking) {
                    window.speechSynthesis.pause();
                } else {
                    window.speechSynthesis.speak(msg);
                }
            }
        }
    }

    setPage(i) {
        if (this._story.isValidPage(i)) {
            if (this.speechEnabled) {
                window.speechSynthesis.cancel();
            }
            this.clear(false);
            this._story.setPage(i);
            this.play();
        }
    }

    nextPage() {
        this.setPage(this._story.currentPageNumber + 1);
    }

    prevPage() {
        this.setPage(this._story.currentPageNumber - 1);
    }

    isPaused() {
        return this._paused;
    }
}
