
function mkel(htmlType, data) {
    let elem = document.createElement(htmlType), txtNode,
    classes = data.classes || [],
    attributes = data.attributes || [],
    children = data.children || [];

    if (data.id) {
        elem.id = data.id;
    }

    for (const hclass of classes) {
        elem.classList.add(hclass);
    }

    if (data.content) {
        txtNode = document.createTextNode(data.content);

        elem.appendChild(txtNode);
    }

    for (const child of children) {
        elem.appendChild(child);
    }

    for (const attrKey in attributes) {
        if (attributes.hasOwnProperty(attrKey)) {
            const attrVal = attributes[attrKey];
            
            elem.setAttribute(attrKey, attrVal);
        }
    }

    return elem;
}

function toggleDisplay(elem) {
    elem.style.display === 'none' ? elem.style.display = '' : elem.style.display = 'none';
}

function byID(eid) {
    return document.getElementById(eid);
}

export {
    mkel, byID, toggleDisplay
}
