import Library from './Library.js';
import WebPlayer from './WebPlayer.js';
import { byID, toggleDisplay } from './utils.js';

const LIBRARY = 1;
const STORY = 2;

export default class Player extends WebPlayer {
    constructor(targetDIV) {
        super(targetDIV);
        this._library = new Library(this);
        this._library.draw();
        this.generateDIVStructure();
        this.switchTo(LIBRARY);
    }

    _initEvents() {
        super._initEvents();
        const controls = byID('player-controls');

        byID('goto-library').addEventListener('click', () => {
            this.switchTo(this._at === LIBRARY ? STORY : LIBRARY);
        });

        document.addEventListener('keyup', event => {
            if (this._at === STORY ) {
                if (event.key === 'ArrowLeft') {
                    this.prevPage();
                } else if (event.key === 'ArrowRight') {
                    this.nextPage();
                } else if (event.key === 'r') {
                    this.speakText();
                } else if (event.key === 'h') {
                    toggleDisplay(byID('instructions'));
                } else if (event.key === 'Escape') {
                    this.switchTo(LIBRARY);
                } else if (event.key === 'p') {
                    this.togglePause();
                } else if (event.key === 's') {
                    this.stop();
                } else if (event.key === 'c') {
                    if (controls.style.display === '') {
                        byID('story-div').style.height = '100%';
                    } else {
                        byID('story-div').style.height = `calc(100% - 64px)`;
                    }
                    toggleDisplay(controls);
                }
            } else if (this._at === LIBRARY) {
                if (event.key === 'Escape') {
                    this.switchTo(STORY);
                }
            }
        });
    }

    switchTo(code) {
        if (code !== this._at) {
            this._at = code;
            if (code !== STORY) {
                this.pause(true);
            }
            if (code === LIBRARY) {
                byID('story-library').style.display = '';
                byID('player-div').style.display = 'none';
            } else if (code === STORY) {
                if (this._paused) {
                    this.pause(false);
                }

                byID('story-library').style.display = 'none';
                byID('player-div').style.display = '';
            }
        }
    }

    load(story) {
        super.load(story);
        this.switchTo(STORY);
    }

}
