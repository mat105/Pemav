const path = require('path');

module.exports = {
  entry: './src/js/WebPlayer.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'pemav.js',
    library: 'WebPlayer',
    libraryTarget: 'window',
    libraryExport: 'default'
  }
};