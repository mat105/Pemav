# Desarrollo
En este directorio se lleva a cabo el desarrollo del prototipo final de reproductor de historias multimedia propuestos por la tesis.

## Breve explicación del diseño
index.html es el punto de entrada, en el se "require" a main.js, el cual crea el Player, el cual inicializará todo y luego creará una biblioteca.
La biblioteca leerá todas las historias disponibles en la carpeta stories y mostrará su título y portada.
Al hacer click en una se carga la historia y se muestra la primer página.
Cada página se compone de elementos, que pueden ser visuales (texto, imágen, video) o no (sonido y música).
Cada elemento maneja su lógica de dibujado (HTML) y reproducción (sonido) en su clase correspondiente.

## Como ejecutarlo
- Descargar NW.js
- Ejecutar "nw.exe ruta/a/esta/carpeta"

## Version empaquetada para mostrar
- Comprimir todo este directorio (sin directorio contenedor, archivos sueltos) en un archivo zip.
- Renombrar el zip a package.nw
- Ponerlo en la carpeta de nw.js
- Ejecutar nw.exe
- *Investigar Web2Executable u otro para que solo se necesite distribuir el exe y no todas las libs y directorios de nw sueltos*

## Version web (para mostrar en un sitio)
- Ejemplo: clarinindex.html (usa test_src/main.js para crear el player)
- Importar pemav.css y pemav.js de /dist.
- Cuando el body este cargado, instanciar un WebPlayer pasandole el id de un div (que tenga fijado width y height en css) y luego llamar a su metodo loadStory(pasandole el json del formato).
- Ej: var st = new WebPlayer('story-div'); st.loadStory(json_pem);
- Si se hacen cambios, hay que ejecutar npm run build

## Ayuda
- *Si se requiere dar soporte a video, es necesario descargar una version alternativa de FFMpeg para dar soporte a más formatos. Este es un dll y se obtiene de https://github.com/iteufel/nwjs-ffmpeg-prebuilt/releases*
- Los controles del player son visibles cuando se esta leyendo una historia (pero se puede presionar H para ver mas funcionalidades y su respectiva tecla de acceso rápido)

## Referencias

- https://electronjs.org/docs
- https://howlerjs.com/
- https://www.valentinog.com/blog/webpack-tutorial/
- http://exploringjs.com/es6/ch_modules.html
