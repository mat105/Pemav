var storyTest = `{
    "metadata": {
        "copyright": {
            "url": "https://www.chiquipedia.com/cuentos-infantiles-cortos/cuentos-populares/el-patito-feo/"
        }
    },
    "story": {
        "options": {
            "allowBrowserSpeech": true
        },
        "metadata": {
            "language": "es-ES",
            "cover": "res/cover.jpg",
            "title": "Patito feo"
        },
        "content": {
            "pages": [
                {
                    "text": {
                        "content": "Al igual que todos los años, en los meses de verano, la Señora Pata se dedicaba a empollar. El resto de las patas del corral siempre esperaban con muchos deseos que los patitos rompiesen el cascarón para poder verlos, pues los patitos de esta distinguida pata siempre eran los más bellos de todos los alrededores.\\nEl momento tan esperado llegó, lo que causó un gran alboroto ya que todas las amigas de mamá pata corrieron hacia el nido para ver tal acontecimiento. A medida que iban saliendo del cascarón, tanto la Señora Pata como sus amigas gritaban de la emoción de ver a unos patitos tan bellos como esos. Era tanta la algarabía que había alrededor del nido que nadie se había percatado que aún faltaba un huevo por romperse.\\nEl séptimo era el más grande de todos y aún permanecía intacto lo que puso a la expectativa a todos los presentes. Un rato más tarde se empezó a ver como el cascarón se abría poco a poco, y de repente salió un pato muy alegre."
                    }
                },
                {
                    "text": {
                        "content": "Cuando todos lo vieron se quedaron perplejos porque este era mucho más grande y larguirucho que el resto de los otros patitos, y lo que más impresionó era lo feo que era.\\nEsto nunca le había ocurrido a la Señora Pata, quien para evitar las burlas de sus amigas lo apartaba con su ala y solo se dedicaba a velar por el resto de sus hermanitos. Tanto fue el rechazo que sufrió el patito feo que él comenzó a notar que nadie lo quería en ese lugar."
                    },
                    "image": {
                        "source": "res/patito.jpg"
                    }
                },
                {
                    "text": {
                        "content": "Toda esta situación hizo que el patito se sintiera muy triste y rechazado por todos los integrantes del coral e incluso su propia madre y hermanos eran indiferentes con él. Él pensaba que quizás su problema solo requería tiempo, pero no era así pues a medida que pasaban los días era más largo, grande y mucho más feo. Además se iba convirtiendo en un patito muy torpe por lo que era el centro de burlas de todos.\\nUn día se cansó de toda esta situación y huyó de la granja por un agujero que se encontraba en la cerca que rodeaba a la propiedad. Comenzó un largo camino solo con el propósito de encontrar amigos a los que su aspecto físico no les interesara y que lo quisieran por sus valores y características.\\nDespués de un largo caminar llegó a otra granja, donde una anciana lo recogió en la entrada. En ese instante el patito pensó que ya sus problemas se habían solucionado, lo que él no se imaginaba que en ese lugar sería peor. La anciana era una mujer muy mala y el único motivo que tuvo para recogerlo de la entrada era usarlo como plato principal en una cena que preparaba. Cuando el patito feo vio eso salió corriendo sin mirar atrás."
                    }
                },
                {
                    "image": {
                        "source": "res/pato.jpg"
                    },
                    "text": {
                        "content": "Pasaba el tiempo y el pobrecillo continuaba en busca de un hogar. Fueron muchas las dificultades que tuvo que pasar ya que el invierno llegó y tuvo que aprender a buscar comida en la nieve y a refugiarse por sí mismo, pero estas no fueron las únicas pues tuvo que esquivar muchos disparos provenientes de las armas de los cazadores."
                    }
                },
                {
                    "text": {
                        "content": "Siguió pasando el tiempo, hasta que por fin llegó la primavera y fue en esta bella etapa donde el patito feo encontró por fin la felicidad. Un día mientras pasaba junto a estanque diviso que dentro de él había unas aves muy hermosas, eran cisnes. Estas tenían clase, eran esbeltas, elegantes y se desplazaban por el estanque con tanta frescura y distinción que el pobre animalito se sintió muy abochornado por lo torpe y descuidado que era él."
                    }
                },
                {
                    "text": {
                        "content": "A pesar de las diferencias que él había notado, se llenó de valor y se dirigió hacia ellos preguntándole muy educadamente que si él podía bañarse junto a ellos. Los cisnes con mucha amabilidad le respondieron todos juntos:\\n– ¡Claro que puedes, como uno de los nuestros no va a poder disfrutar de este maravilloso estanque!\\n\\nEl patito asombrado por la respuesta y apenado les dijo:\\n– ¡No se rían de mí! Como me van a comparar con ustedes que están llenos de belleza y elegancia cuando yo soy feo y torpe. No sean crueles burlándose de ese modo.\\n– No nos estamos riendo de ti, mírate en el estanque y veras como tu reflejo demostrara cuan real es lo que decimos.- le dijeron los cisnes al pobre patito.\\nDespués de escuchar a las hermosas aves el patito se acercó al estanque y se quedó tan asombrado que ni el mismo lo pudo creer, ya no era feo. ¡Se había transformado en un hermoso cisne durante todo ese tiempo que pasó en busca de amigos! Ya había dejado de ser aquel patito feo que un día huyó de su granja para convertirse en el más bello y elegante de todos los cisnes que nadaban en aquel estanque."
                    }
                }
            ]
        }
    }
}`;

window._pemav = new WebPlayer('player-div');

setTimeout(function() {
    window._pemav.loadStory(storyTest, 'stories/st02');
}, 200);
