# Tesis

Tesis sobre un reproductor de historias multimedia.

## ¿Que es una historia multimeda?
Una historia/cuento/relato/etc compuesto de una serie de páginas que pueden contener, texto, animación, imágen, video y/o sonido.

## ¿Como funciona?
Existe un formato (ver /doc/format) que describe el contenido de la historia, tanto su metadata como la descripción de los elementos que componen cada una de las páginas.

El reproductor a desarrollar es capaz de leer este formato y presentarlo al usuario para su "consumo"

## ¿Como ejecutar el reproductor?

Atención: Puede ahorrarse los pasos 2, 3 y parte del 4 a continuación, descargando este reproductor ya comprimido (que es un package.nw). Ver el archivo LEEME.txt dentro de pemav para los pasos reducidos.
http://s000.tinyupload.com/index.php?file_id=03156965796360263378

1) Descargar y descomprimir nw.js:
https://dl.nwjs.io/v0.39.2/nwjs-v0.39.2-win-x64.zip
2) Descargar y descomprimir este repositorio de codigo:
https://gitlab.com/mat105/Pemav/-/archive/master/Pemav-master.zip?path=pemav
3) Ejecutar npm install en la carpeta descomprimida de pemav.
4) Hacer solo uno de los siguientes pasos:
   - Copiar y pegar todos los archivos de la carpeta pemav dentro de la carpeta donde se descomprimio nw.js
   - Comprimir todos los archivos de la carpeta dentro de un .zip con nombre package, pero cambiando la extensión del archivo .zip a .nw. Es decir el archivo debe estar comprimido como zip y llamarse package.nw
   - Renombrar la carpeta pemav a package.nw y pegarla en el directorio de nw.js.
   - Arrastrar y soltar la carpeta pemav sobre el ejecutable de nw.js (nw.exe).
5) Correr el ejecutable (nw.exe)
6) Opcional (para poder reproducir video): Descargar las bibliotecas de ffmpeg y pegarlas dentro de la carpeta de nw.js: https://github.com/iteufel/nwjs-ffmpeg-prebuilt/releases/download/0.38.1/0.38.1-win-ia32.zip
