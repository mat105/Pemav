# Estructura

Version 1.0

2/7/2018

## Contenidos

* [1 Introducción](#1)
* [2 Conceptos básicos](#2)
* [3 Elementos](#3)
    * [3.1 Texto](#3.1)
    * [3.2 Imágen](#3.2)
    * [3.3 Sonido](#3.3)
    * [3.4 Video](#3.4)
* [4 Formato](#4)
    * [4.1 Metadata](#4.1)
    * [4.2 Historia](#4.2)
    * [4.3 Metadata](#4.3)
    * [4.4 Contenido](#4.4)
<br/>

# <a name="1"/>1 Introducción

El formato "pem" es un formato diseñado para la representación de historias multimedia basadas en páginas, el formato brinda soporte a la declaración de elementos de tipo texto, imágen, sonido y video.

# <a name="2"/>2 Conceptos básicos

Una historia de pem será entendida como una serie de páginas compuestas de elementos multimedia. El prototipo de Pem simplifica esta composición, forzando a que solo pueda existir 1 elemento gráfico de cada tipo por página (1 imágen y 1 texto, 1 texto, 1 video y 1 texto, etc).
<br/>
Cada uno de estos elementos es configurable con parámetros que modifican su estilo (color, tamaño, etc). Pem no especifica de que forma de reproducen las páginas (si es lineal, hay saltos u otras formas).

# <a name="3"/>3 Elementos

## <a name="3.1"/>3.1 Texto
El texto de la página, soporta distintos tamaños, colores, saltos de líneas y wrapping al alcanzar el tamaño máximo de la pantalla.

## <a name="3.2"/>3.2 Imágen
La imágen de la página, puede indicarsele si expandirse por ratio o cubrir lo máximo posible.

## <a name="3.3"/>3.3 Sonido
Define un efecto de sonido, puede indicarsele un tiempo de retraso para el inicio de su reproducción y el volumen.

## <a name="3.4"/>3.4 Video
El video de la página, puede indicarsele si expandirse por ratio o cubrir lo máximo posible, además de poder modificarse su volumen.

# <a name="4"/>4 Formato

Pem define su estructura como un JSON.

```
{
    "metadata" : {...},
    "story" : {...}
}
```

## <a name="4.1"/>4.1 Metadata

La metadata propia del formato en sí (archivo) y no de la historia.

```
{
    "name": "Pem",
    "version" : "1.0",
    "dateOfCreation" : "2018-06-27"
}
```

## <a name="4.2"/>4.2 Historia

Estructura de la historia.

```
{
    "metadata" : {...},
    "content": {...}
}
```
## <a name="4.3"/>4.3 Metadata

Metadata de la historia.

```
{
    "author" : "Matias",
    "title" : "Cuentito",
    "date" : "2018-06-27",
    "description" : "2018-06-27"
}
```

## <a name="4.4"/>4.4 Contenido

Aquí se declara el contenido de la historia.

```
{
    "pages" : [
        {
            "text" : {
                "color" : "yellow",
                "content": "Esto es un texto\nCon dos lineas."
            }
        },
        {
            "image" : {
                "source" : "imagen.png",
            },
            "sound" : {
                "source" : "sonido.mp3"
            }
        }
    ]
}
```
