
/*  Eventos y estados*/
EVENT_EXPAND = 1;
EVENT_DISABLE_MOUSEDOWN = 2;
ALL_EVENTS = [EVENT_EXPAND, EVENT_DISABLE_MOUSEDOWN];

INITIAL = 1;
PLAYING = 2;
PAUSED = 3;

/* Funciones de utilidad */
function byID(id) {
    return document.getElementById(id);
}

/* Si el browser soporta text to speech usarlo si la historia ademas lo permite */
function speechText(txt) {
    
    if (_pemav.speechEnabled()) {
        var msg = new SpeechSynthesisUtterance(txt);
        msg.lang = _pemav.getStoryLanguage();
        window.speechSynthesis.cancel();

        window.speechSynthesis.speak(msg);
    }
}

/* Clase abstracta de la que heredan los demas elementos */
class Element {
    play() {}
    pause() {}
    clear() {}
    unpause() {
        this.play();
    }

    canPlay() {
        return this.page == _pemav.story.getCurrentPage();
    }

    click(event) {
        _pemav.toggleElementsHideExcept(this);
    }

    constructor(page, element) {
        this.page = page;
    }

    mouseDown(event) {
        event.preventDefault();
    }
}

class VisibleElement extends Element {
    getContainerID() {
        return null;
    }

    getHTMLElement() {
        return null;
    }

    constructor(page, element) {
        super(page, element)
    }

    /* El elemento debe estar en el dom para que se registren los eventos */
    registerEvents(events) {
        var that = this;
        let elem = this.getHTMLElement();
        events = events || [];

        if (elem) {
            for (let ev of events) {
                if (ev === EVENT_EXPAND)
                    elem.addEventListener('dblclick', function(ev) { that.click(ev) });
                else if (ev === EVENT_DISABLE_MOUSEDOWN)
                    elem.addEventListener('mousedown', function(ev) { that.mouseDown(ev) });
            }
            
        } else {
            console.log("Eventos no generado para elemento dentro de: ", this.getContainerID());
        }
    }

    draw() {}
}

class TextElement extends VisibleElement {
    constructor(page, element) {
        super(page, element);
        this.text = element.content;
    }

    /* Temporalmente usa el propio container */
    getHTMLElement() {
        return byID('story-text');
    }

    getContainerID() {
        return 'text-container';
    }

    clear() {
        window.speechSynthesis.cancel();
    }

    draw() {
        let textElement;
        var that = this;

        if (!byID('story-text')) {
            textElement = document.createElement('div');
            textElement.id = 'story-text';
            textElement.innerHTML = this.text;

            this.read_text = textElement.innerText;

            byID(this.getContainerID()).appendChild(textElement);
            this.registerEvents([EVENT_EXPAND]);
        }
    }

    pause() {
        if (_pemav.speechEnabled()) {
            if (window.speechSynthesis.speaking) {
                window.speechSynthesis.pause();
            }
        }
    }

    unpause() {
        if (_pemav.speechEnabled()) {
            window.speechSynthesis.resume();
        }
    }

    /* Lograr que cuando el sonido termine se narre el texto */
    play() {
        if (!this.page.sound) {
            speechText(this.read_text);
        }
    }
}

class SoundElement extends Element {
    constructor(page, element) {
        super(page, element);
        this.sound = element.source;
        this.volume = element.volume || 1;

        this.preload();
    }

    isPlaying() {
        return this.soundInstance != null && this.soundInstance.playing();
    }

    preload() {
        this.soundInstance = new Howl({
            src: [this.sound],
            volume: this.volume
        });
    }

    play() {
        this.soundInstance.play();
    }

    clear() {
        this.soundInstance.off('end');
        this.soundInstance.stop();
    }

    unpause() {
        if (this.wasPlaying) {
            this.soundInstance.play();
        }
    }

    pause() {
        this.wasPlaying = this.isPlaying();
        this.soundInstance.pause();
    }
}

class ImageElement extends VisibleElement {

    constructor(page, element) {
        super(page, element);
        this.image = element.source;
    }
    getContainerID() {
        return 'visual-container';
    }

    getHTMLElement() {
        return byID('story-image');
    }

    draw() {
        let imageElement, scene;

        /* Revisar si mover creacion al constructor y que draw solo haga el append */
        if (!byID('story-image')) {
            imageElement = document.createElement('img');
            imageElement.id = 'story-image';
            imageElement.src = this.image;
            imageElement.alt = 'La imagen no puede visualizarse.';

            byID(this.getContainerID()).appendChild(imageElement);
            this.registerEvents([EVENT_EXPAND, EVENT_DISABLE_MOUSEDOWN]);
        }
    }
}

class VideoElement extends ImageElement {
    constructor(page, element) {
        super(page, element);
        this.video = element.source;
        this.volume = element.volume || 1;
        this.startPosition = element.startPosition || 0;

        this.preload();
    }

    preload() {
        /* Esto solo deberia hacerse si la pagina esta proxima a ser mostrada (distancia de 1-2 paginas) */
        this.videoElement = document.createElement('video');
        this.videoElement.id = 'story-video';
        this.videoElement.src = this.video;
        this.videoElement.alt = 'El video no puede visualizarse.';
        this.videoElement.preload = true;
        this.videoElement.volume = this.volume;
        this.videoElement.currentTime = this.startPosition;
    }

    getHTMLElement() {
        return byID('story-video');
    }

    clear() {
        this.videoElement.pause();
        this.videoElement.currentTime = this.startPosition;
    }

    pause() {
        this.videoElement.pause();
    }

    play() {
        this.getHTMLElement().play();
    }

    draw() {
        if (!byID('story-image')) {
            byID(this.getContainerID()).appendChild(this.videoElement);
            this.registerEvents([EVENT_EXPAND, EVENT_DISABLE_MOUSEDOWN]);
        }
    }
}

class Page {
    constructor(page) {
        var elems = {"text": TextElement, "image": ImageElement, "sound": SoundElement, "video": VideoElement};
        this.elements = [];

        for (var k in page) {
            if (elems[k] != null) {
                let elem = new elems[k](this, page[k]);
                this.elements.push(elem);
                this[k] = elem;
            }
        }
    }

    toggleElementsHideExcept(elem) {
        document.querySelectorAll(`.container:not(#${elem.getContainerID()}`).forEach((el) => el.style.display = el.style.display === 'none' ? '' : 'none');
    }

    play() {
        this.elements.forEach(element => {
            element.play();
        });
    }

    pause() {
        this.elements.forEach(element => {
            if (element.pause) {
                element.pause();
            }
        });
    }

    unpause() {
        this.elements.forEach(element => {
            if (element.unpause) {
                element.unpause();
            }
        });
    }

    draw() {
        this.elements.forEach(element => {
            if (element.draw) {
                element.draw();
                byID(element.getContainerID()).style.display = '';
            }
        });
    }
}

/* La clase encargada de traducir el formato en entidades */
class Story {
    constructor(storyData) {
        this.content = storyData.content;
        this.options = storyData.options;
        this.metadata = storyData.metadata;

        this.pages = storyData.content.pages.map((page) => new Page(page));
        this.current = 0;
    }

    clear() {
        this.pages[this.current].elements.forEach(e => e.clear());

        document.querySelectorAll('.container').forEach(e => {
            e.innerHTML = '';
            e.style.display = 'none';
        });
    }

    getCurrentPage() {
        return this.pages[this.current];
    }

    setPage(i) {
        if (i >= 0 && i < this.pages.length) {
            this.clear();
            this.current = i;
            this.draw();
            this.play();
        }
    }

    getLanguage() {
        return this.metadata.language || 'es-ES';
    }

    nextPage() {
        this.setPage(this.current + 1);
    }

    prevPage() {
        this.setPage(this.current - 1);
    }

    pause() {
        this.getCurrentPage().pause();
    }

    unpause() {
        this.getCurrentPage().unpause();
    }

    draw() {
        this.getCurrentPage().draw();
    }

    /* Verificar si hacer un timeline de reproduccion (es decir sonido->narracion) */
    play() {
        this.getCurrentPage().play();
    }
}

/* El player */
class Pemav {
    constructor(pemData) {
        this.pem = JSON.parse(pemData);
        this._browserSpeech = 'speechSynthesis' in window;
        this._state = INITIAL;
    }

    speechEnabled() {
        if (this.story) {
            return this._browserSpeech && this.story.options.allowBrowserSpeech != null;
        }

        return this._browserSpeech;
    }

    getStoryLanguage() {
        return this.story.getLanguage();
    }

    toggleElementsHideExcept(elem) {
        this.story.pages[this.story.current].toggleElementsHideExcept(elem);
    }

    nextPage() {
        this.story.nextPage();
    }

    prevPage() {
        this.story.prevPage();
    }

    pause() {
        this.state = PAUSED;
        this.story.pause();
    }

    unpause() {
        this.state = PLAYING;
        this.story.unpause();
    }

    togglePause() {
        this.state === PLAYING ? this.pause() : this.unpause();
    }

    play() {
        this.state = PLAYING;
        this.story = new Story(this.pem.story);
        this.story.setPage(0);
    }
}

var _data = `{
    "story": {
        "options": {
            "allowBrowserSpeech": true
        },
        "metadata": {
            "language": "es-ES"
        },
        "content": {
            "pages": [
                {
                    "text": {
                        "content": "<p>Esto es un texto: <b>¡Hola!</b></p><p>Para ir a la siguiente página, presione la flecha derecha en la parte inferior de la pantalla.</p>"
                    }
                },
                {
                    "image": {
                        "source": "res/forest.jpg"
                    }
                },
                {
                    "text": {
                        "content": "También pueden mostrarse texto e imagen a la vez."
                    },
                    "image": {
                        "source": "res/panda.jpg"
                    }
                },
                {
                    "video": {
                        "source": "res/bears.mp4"
                    },
                    "text": {
                        "content": "En esta página puede verse un video acompañado de un elemento de texto."
                    }
                }
            ]
        }
    }
}`

var _pemav = new Pemav(_data);
